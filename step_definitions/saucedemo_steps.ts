const { I, catalogPage, cartPage } = inject();

import clonedeep from 'lodash.clonedeep';

Given('I go to the main saucedemo page', async () => {
  I.amOnPage("https://www.saucedemo.com");
  I.clearCookie();
});

Then('I am on the main saucedemo page', async () => {
  await catalogPage.assertCatalogPageIsVisible();
});

When('I login as a {string}', async (userid:string) => {
  await I.login(userid);
});

When('I add all items to the shopping cart', async () => {
  await catalogPage.addAllItemsFromCatalogPageToCart();
});

When('I go to the shopping cart', async () => {
  await catalogPage.goToTheShoppingCart()
});

When('I remove {string} item found by name from the cart', async (index:string) => {
  const idx = Number.parseInt(index);
  await cartPage.removeItemFoundByNameWithIndex(idx);
});

Then('item total count is {int}', async (total:number) => {
  await cartPage.assertItemTotalCountIs(total);
});

When('I checkout with next purchase data:', async(table) => {
  const checkoutInfo = table.parse().hashes()[0];  
  await I.checkout(checkoutInfo.firstName, checkoutInfo.lastName, checkoutInfo.zipCode);
})

Then('saucedemo confirms successful purchase', async () => {
  await I.seePurchaseCompletedSuccessfully();
});


When('sort item catalog by {string} criteria', async (criteria:string) => {
  await catalogPage.sortItemCatalogPageBy(criteria);
});

Then('catalog items sorted correctly by price in {string} order', async (order:string) => {
  await catalogPage.assertItemCatalogPageSortedByPrice(order);
});

Then('catalog items sorted correctly by name in {string} order', async (order:string) => {
  await catalogPage.assertItemCatalogPageSortedByName(order);  
});

Then('I see authentication error details message', async () => {
  await I.seeAuthenticationErrorMessage();  
})

When('I browse details for {string} item', async (title:string) => {
  await catalogPage.browseItemDetails(title);  
})

When('I add browsed item to the shopping cart', async () => {
  await I.addBrowsedItemToCart();  
})

Then('shopping cart contains next items:', async (table) => {
  const tableByHeader = table.parse().hashes();
  for (const row of tableByHeader) {
    await cartPage.assertCartContainsItem(row.title);
  }  
})
