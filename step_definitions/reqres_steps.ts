const { I } = inject();

import { log } from "console";
import { USERS_ENDPOINT, LOGIN_ENDPOINT } from "../constants/Endpoints"; 

let resp;
let respPromise:Promise<any>[];
let query:string = "";
let responseTime:number = 0;
let userUpdatePayload = {};

Given('query parameter {string} with value {string} to be used in request', async (key:string, value:string) => {
    query = '?' + key + '=' + value;
});

When('I get all users', async () => {
    resp = await I.sendGetRequest(USERS_ENDPOINT);
});

When('I get users with query parameters', async () => {
    resp = await I.sendGetRequest(USERS_ENDPOINT + query);
});

When('I login with {string} with {string} password', async (email:string, password:string) => {
    const loginPayload = {email: email, password: password };
    resp = await I.sendPostRequest(LOGIN_ENDPOINT, loginPayload);
});

When('I create a user with following data:', async (table) => {
    const userData = table.parse().hashes()[0];
    resp = await I.sendPostRequest(USERS_ENDPOINT, userData);
});

When('I update the user with the next data:', async (table) => {
    const userData = table.parse().hashes()[0];
    userUpdatePayload = {name: userData.name, job: userData.job};
    resp = await I.sendPutRequest(USERS_ENDPOINT + "/" + userData.id, userUpdatePayload);
});

When('I get user with {string} id', async (id:string) => {
    let args = [USERS_ENDPOINT + "/" + id];
    resp = await I.measureResponseTimeFor(I.sendGetRequest, args)
    responseTime = resp.responseTime / 1000;
});

When('I get async {int} users', (count:number) => {
    respPromise = 
        Array(count).fill(0).map((_, i) => i + 1).map( i => I.sendGetRequest(USERS_ENDPOINT + "/" + i));
});

Then('response code is {int}', async (code:number) => {
    I.seeResponseCodeIs(code);
});

Then('async response code for each is {int}', async (code:number) => {
    (await Promise.all(respPromise)).forEach((resp) => {I.assertEqual(resp.status, code);});
});

Then('the response body matches request body where applicable', async () => {
    const userUpdateResponse = resp.data;
    I.assertDeepEqualExcluding(userUpdateResponse, userUpdatePayload,'updatedAt');
});

Then('user creation date is todays date', async () => {
    const createdAt = resp.data.createdAt;
    const today = new Date().toISOString();
    I.assertEqual(createdAt.split('T')[0] , today.split('T')[0]);
});

Then('user for requested id not found', async () => {
    I.dontSeeResponseContainsJson({data: []});
});

Then('response time is less than {int} seconds', async (seconds:number) => {
    I.assertTrue(responseTime < seconds);    
});

Then('login have been failed', async () => {
    I.assertNotContain(JSON.stringify(resp.data), 'token');
});

Then('the error is {} message', async (message:string) => {
    I.assertContain(JSON.stringify(resp.data), message);
});

Then('I print all users with odd ids', async () => {
    const users = resp.data.data;
    users.filter(user => user.id % 2 == 1)
            .forEach(user => console.log(user));
});