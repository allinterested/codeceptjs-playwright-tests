Feature: Saucedemo validations
  In order to be more proficient with CodeceptJS/Playwright UI testing
  As a test automation engineer
  I want to automate through UI with codeceptjs-playwright following scenarios

  Background: Go to main saucedemo page
    Given I go to the main saucedemo page

  Scenario Outline: Verify order purchase
    When I login as a '<user>'
    Then I am on the main saucedemo page
    When I add all items to the shopping cart
    And I go to the shopping cart
    And I remove '3' item found by name from the cart
    Then shopping cart contains next items:
      | title                             |
      | Sauce Labs Backpack               |
      | Sauce Labs Bike Light             |
      | Sauce Labs Fleece Jacket          |
      | Sauce Labs Onesie                 |
      | Test.allTheThings() T-Shirt (Red) |
    And item total count is 5
    When I checkout with next purchase data:
      | firstName | lastName  | zipCode |
      | Ivan      | Paulouski | 50-513  |
    Then saucedemo confirms successful purchase
    Examples:
      | user                    |
      | standard_user           |
      | performance_glitch_user |

  #Reveals a bug for catalog/details name mapping 
  Scenario: Verify shopping cart feature
    When I login as a 'problem_user'
    Then I am on the main saucedemo page
    When I browse details for 'Sauce Labs Bolt T-Shirt' item
    And I add browsed item to the shopping cart
    And I go to the shopping cart
    Then shopping cart contains next items:
      | title                   |
      | Sauce Labs Bolt T-Shirt |

  Scenario: Verify item catalog sorting feature by name
    When I login as a 'standard_user'
    Then I am on the main saucedemo page
    When sort item catalog by 'Name (A to Z)' criteria
    Then catalog items sorted correctly by name in 'asc' order

  Scenario: Verify item catalog sorting feature by price
    When I login as a 'standard_user'
    Then I am on the main saucedemo page
    When sort item catalog by 'Price (low to high)' criteria
    Then catalog items sorted correctly by price in 'asc' order

  Scenario: Verify locked user login restrictions
    When I login as a 'locked_out_user'
    Then I see authentication error details message
