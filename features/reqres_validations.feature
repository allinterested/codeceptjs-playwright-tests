Feature: Reqres validations
    In order to be more proficient with CodeceptJS/Playwright API testing
    As a test automation engineer
    I want to automate through API with CodeceptJS/Playwright following scenarios

    Scenario: Get users and print ones with odd id's
        When I get all users
        Then response code is 200
        And I print all users with odd ids

    Scenario: Get user with non-existing id and verify not found handling
        When I get user with '13' id
        Then response code is 404
        And user for requested id not found

    Scenario: Create new user and verify creation date is today
        When I create a user with following data:
            | name | job                      |
            | Ivan | Test Automation Engineer |
        Then response code is 201
        And user creation date is todays date

    Scenario: Update user and verify update occured
        When I update the user with the next data:
            | id | name | job     |
            | 2  | Jim  | Student |
        Then response code is 200
        And the response body matches request body where applicable

    Scenario Outline: Get users and verify response time
        Given query parameter 'delay' with value '<value>' to be used in request
        When I get users with query parameters
        Then response time is less than 1 seconds
        Examples:
            | value |
            | 0     |
            | 3     |

    Scenario: Get 10 users and verify success status async
        When I get async 10 users 
        Then async response code for each is 200

    Scenario: Login without password and verify error message
        When I login with 'user1@mail.com' with '' password
        Then response code is 400
        And login have been failed
        And the error is "Missing password" message