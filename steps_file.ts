// in this file you can append custom step methods to 'I' object

export = function() {
  return actor({
    login: function(userid) {
      this.say(`Login with '${userid}' userid`);
      this.fillField('Username', userid);
      this.fillField('Password', 'secret_sauce');
      this.pressKey('Enter');
    },
    checkout: function(firstName, lastName, zipCode) {
      this.say(`Checking out with next info: ${firstName} ${lastName}, code: ${zipCode}`);
      this.click('Checkout');
      this.fillField('#first-name', firstName);
      this.fillField('#last-name', lastName);
      this.fillField('#postal-code', zipCode);
      this.click('Continue');
      this.click('Finish');
    },
    seeAuthenticationErrorMessage: function() {
      this.say('Verifying authentication error message is displayed');
      this.seeElement('.error-message-container');
    },
    seePurchaseCompletedSuccessfully: function() {
      this.say('Verifying purchase completed successfully');
      this.seeTextEquals('Thank you for your order!', '.complete-header');
    },
    addBrowsedItemToCart: function() {
      this.say('Adding browsed item to cart');
      this.click('.btn_inventory');
    },
    measureResponseTimeFor: async function(fun: (...args:any[]) => any, params:any[]) {
      let begin = Date.now();
      let resp = await fun(...params);
      let end = Date.now();
      let responseTime = end - begin;
      resp.responseTime = responseTime;
      return resp;
    }
  });
}
