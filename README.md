<h2>Test automation framework with CodeceptJS-Playwright toolkit</h2>

<h3>Goals</h3>
<p>Practice UI and API test automation using TypeScript/Playwright/CodeceptJS</p>
<h3>Main features</h3>
<ul>
<li>Report with screenshots for test failures in output folder</li>
<li>Page objects for domain specific language introduction</li>
<li>Logging values which used in test for robust root cause analysis</li>
<li>Common tasks added directly to actor I</li>
<li>Separate configs for UI and API tests (use -c <i>config_name</i> in run command)</li>
<li>GitLab CI pipeline with essential steps to run tests and save test results as execution artifacts</li>
</ul>
<h3>How to run</h3>
<ol>
<li><b>npm install</b></li>
<li><b>npx playwright install --with-deps</b></li>
<li><b>npx codeceptjs run --features --reporter mochawesome</b></li>
</ol>


