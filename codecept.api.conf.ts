import {
  setHeadlessWhen,
  setCommonPlugins
} from '@codeceptjs/configure';
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();
export const config: CodeceptJS.MainConfig = {
  tests: './test/*_test.ts',
  output: './output',
  gherkin: {
    features: './features/reqres_validations.feature',
    steps: [
      './step_definitions/reqres_steps.ts',
    ]
  },
  mocha: {
    reporterOptions: {
      reportDir: "output"
    }
  },
  helpers: {
    REST: {
      endpoint: "https://reqres.in/api/",
      defaultHeaders: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    },
    JSONResponse: {

    },
    ChaiWrapper: {
      require: 'codeceptjs-chai'
    },
    Mochawesome: {
      uniqueScreenshotName: 'true'
    }
  },
  include: {
    I: './steps_file',
  },
  name: 'codeceptjs-playwright-tests'
}