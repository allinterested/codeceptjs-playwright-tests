const { I } = inject();
const eachElement = codeceptjs.container.plugins('eachElement');

import clonedeep from 'lodash.clonedeep';
export = {

  itemDetailsLink: '.inventory_item_name',
  shoppingCartLink:'.shopping_cart_link',
  addItemToCartButton: '.btn_inventory',
  sortOptionSelect: '.product_sort_container',
  itemPriceField: '.inventory_item_price',
  productsHeading: '.title',

  async browseItemDetails(title:string) {
    I.say(`Browsing ${title} details`)
    await I.click(locate(this.itemDetailsLink).withText(title));   
  },

  async goToTheShoppingCart() {
    I.say('Go to shopping cart');
    await I.click(this.shoppingCartLink);
  },

  async addAllItemsFromCatalogPageToCart() {
    I.say('Adding all items from catalog page to the cart');
    await eachElement('Add all items from catalog page to cart', this.addItemToCartButton, async el => {
      await el.click()
    });
  },

  async assertCatalogPageIsVisible() {
    I.say(`Verify item catalog page is visible`);
    await I.seeTextEquals('Products', this.productsHeading);
  },

  async sortItemCatalogPageBy(criteria:string) {
    I.say(`Sort item catalog page by ${criteria}`);
    await I.selectOption(this.sortOptionSelect, criteria);
  },

  async assertItemCatalogPageSortedByPrice(order:string) {
    I.say(`Verify item catalog page sorted by price in ${order} order`);
    let itemProperties= (await I.grabTextFromAll(this.itemPriceField))
      .map(pr => Number.parseFloat(pr.split('$')[1]));
    let comparator = (a:number, b:number) => a - b;
    if (order === "desc") {
      comparator = (a:number, b:number) => b - a;
    }
    let itemPropertiesSorted = clonedeep(itemProperties).sort(comparator);
    await I.assertEqual(JSON.stringify(itemProperties), JSON.stringify(itemPropertiesSorted));  
  },

  async assertItemCatalogPageSortedByName(order:string) {
    I.say(`Verify item catalog page sorted by name in ${order} order`);
    let itemProperties = await I.grabTextFromAll(this.itemDetailsLink);
    let itemPropertiesSorted = clonedeep(itemProperties).sort();
    if (order === "desc") {
      itemPropertiesSorted.reverse();
    }
    await I.assertEqual(JSON.stringify(itemProperties), JSON.stringify(itemPropertiesSorted));
  },

  // insert your locators and methods here
}
