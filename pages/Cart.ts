const { I } = inject();
const eachElement = codeceptjs.container.plugins('eachElement');

export = {

  itemDetailsLink: '.inventory_item_name',
  cartItemLabel: '.cart_item_label',
  itemDetailsTitle: '.inventory_item_name',
  itemCartQuantity: '.cart_quantity', 

  async removeItemFoundByNameWithIndex(index:number) {
    I.say(`Remove item #${index} from cart`);
    let items = await I.grabTextFromAll(this.itemDetailsLink);
    let itemToRemove = items[index - 1];
    await I.click(locate('button').inside(
        locate(this.cartItemLabel).withDescendant(this.itemDetailsTitle).withText(itemToRemove)
      )
    );
  },

  async getCartItemsTitles():Promise<string[]> {
    return await I.grabTextFromAll(this.itemDetailsLink);
  },

  async assertItemTotalCountIs(total:number) {
    I.say(`Verify cart items total count is ${total}`);
    let currentTotal = 0;
    await eachElement('Grab item total', this.itemCartQuantity, async el => {
        currentTotal += Number.parseInt(await el.textContent());
    });
    await I.assertEqual(total, currentTotal);
  },

  async assertCartContainsItem(title:string) {
    I.say(`Verify cart contains ${title} item`);
    let cartItems = await this.getCartItemsTitles();
    I.assertContain(cartItems, title);
  }


}
