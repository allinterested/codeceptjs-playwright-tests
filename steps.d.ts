/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file');
type catalogPage = typeof import('./pages/Catalog');
type cartPage = typeof import('./pages/Cart');
type ChaiWrapper = import('codeceptjs-chai');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, catalogPage: catalogPage, cartPage: cartPage }
  interface Methods extends Playwright, REST, JSONResponse, ChaiWrapper, Mochawesome {}
  interface I extends ReturnType<steps_file>, WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
