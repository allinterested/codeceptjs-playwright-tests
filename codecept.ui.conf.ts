import {
  setHeadlessWhen,
  setCommonPlugins
} from '@codeceptjs/configure';
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();
export const config: CodeceptJS.MainConfig = {
  tests: './test/*_test.ts',
  output: './output',
  gherkin: {
    features: './features/saucedemo_validations.feature',
    steps: [
      './step_definitions/saucedemo_steps.ts',
    ]
  },
  mocha: {
    reporterOptions: {
      reportDir: "output"
    }
  },
  helpers: {
    Playwright: {
      browser: 'chromium',
      url: 'http://localhost',
      show: false,
      uniqueScreenshotNames: true,
      waitForTimeout: 20,
      chromium: {
        args: [
          '--disable-blink-features=AutomationControlled'
        ]
      }
    },
    REST:{

    },
    JSONResponse: {

    },
    ChaiWrapper: {
      require: 'codeceptjs-chai'
    },
    Mochawesome: {
      uniqueScreenshotName: 'true'
    }
  },
  plugins: {
    eachElement: {
      enabled: true
    }
  },
  include: {
    I: './steps_file',
    catalogPage: "./pages/Catalog.ts",
    cartPage: "./pages/Cart.ts",
  },
  name: 'codeceptjs-playwright-tests'
}