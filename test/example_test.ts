Feature('example');

import clonedeep from 'lodash.clonedeep'

const eachElement = codeceptjs.container.plugins('eachElement');
const ITEM_TITLE_LINK = '.inventory_item_name';

Before(async ({ I }) => {
    I.amOnPage("https://www.saucedemo.com");
    I.clearCookie();
})

Scenario('Validation #1',  async ({ I }) => {
    I.login('performance_glitch_user');
    I.seeTextEquals('Products', '.title');
    await eachElement('Add all items', '.btn_inventory', async el => {
        await el.click()
    });
    I.click('.shopping_cart_link');
    let items = await I.grabTextFromAll(ITEM_TITLE_LINK);
    let itemToRemove = items[2];
    const itemTotal = 5;
    I.click({xpath: `//div[contains(text(),'${itemToRemove}')]//..//..//button`});
    I.seeNumberOfVisibleElements('.inventory_item_name', items.length - 1);
    //Veryfing item total
    let currentTotal = 0;
    await eachElement('Grab item total', '.cart_quantity', async el => {
        currentTotal += Number.parseInt(await el.textContent());
    });
    I.assertEqual(itemTotal, currentTotal);
    //checkout
    I.checkout('Ivan', 'Paulouski', '50-5143');
    I.click('Finish');
    I.seeTextEquals('Thank you for your order!', '.complete-header');
}).tag('@ip');


Scenario('Validation #2', async ({ I }) => {
    I.login('problem_user');
    const item = 'Sauce Labs Bike Light'
    I.click({xpath: `//div[@class='inventory_item_name' and .='${item}']`});
    I.click('.btn_inventory');
    I.gotoCart();
    I.seeTextEquals(item, ITEM_TITLE_LINK);
}).tag('@bug');

Scenario('Validation #3', async ({ I }) => {
    I.login('standard_user');
    I.selectOption('.product_sort_container','Name (Z to A)');
    const itemTitles = await I.grabTextFromAll(ITEM_TITLE_LINK);
    const itemTitlesSorted = clonedeep(itemTitles).sort().reverse();
    I.assertEqual(JSON.stringify(itemTitles), JSON.stringify(itemTitlesSorted));
}).tag('@done');

Scenario('Validation #4', async ({ I }) => {
    I.login('standard_user',);
    I.selectOption('.product_sort_container','Price (low to high)');
    const itemPrices = (await I.grabTextFromAll('.inventory_item_price'))
        .map(pr => Number.parseFloat(pr.split('$')[1]));
    let itemPricesSorted = clonedeep(itemPrices).sort((a,b) => a - b);
    I.assertEqual(JSON.stringify(itemPrices), JSON.stringify(itemPricesSorted));
}).tag('@done');